DROP DATABASE IF EXISTS lez29_mobilificio;
CREATE DATABASE lez29_mobilificio;
USE lez29_mobilificio;

CREATE TABLE Categoria(
	categoriaId INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nomeCategoria VARCHAR(250) NOT NULL UNIQUE,
    descrizioneCategoria TEXT,
    codiceCategoria VARCHAR(20) NOT NULL UNIQUE
);

CREATE TABLE Oggetto(
	oggettoId INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nomeOggetto VARCHAR(250) NOT NULL,
    descrizioneOggetto TEXT,
    codiceOggetto VARCHAR(20) NOT NULL UNIQUE,
    prezzoOggetto FLOAT DEFAULT 0.0
);

CREATE TABLE OggettoCategoria(
	oggetto_Id INTEGER NOT NULL,
    categoria_Id INTEGER NOT NULL,
    PRIMARY KEY(oggetto_Id, categoria_Id),
	FOREIGN KEY (oggetto_Id) REFERENCES oggetto(oggettoId),
    FOREIGN KEY (categoria_Id) REFERENCES categoria(categoriaId)
);

INSERT INTO Categoria (nomeCategoria, descrizioneCategoria, codiceCategoria) VALUES
("Sedie", "Sedie da interni", "CAT001"),
("Tavoli", "Tavoli da interni", "CAT002"),
("Armadi", "Armadi da interni", "CAT003"),
("Poltrone", "Poltrone da interni", "CAT004");

INSERT INTO Oggetto (nomeOggetto, descrizioneOggetto, codiceOggetto, prezzoOggetto) VALUES
("Pina", "Sedia/Poltrona da interno", "SED001", 20.0),
("Lino", "Tavolino da caffe", "TAV001", 45.0),
("Armadillo", "Armadio porta-oggetti", "ARM001", 120.0);

INSERT INTO OggettoCategoria (oggetto_Id, categoria_Id) VALUES
(1,1),
(1,4),
(2,2),
(3,3);

-- SELECT * FROM Categoria;
-- SELECT * FROM OggettoCategoria;

SELECT * 
	FROM Categoria
    JOIN OggettoCategoria ON Categoria.categoriaId = OggettoCategoria.categoria_Id
    JOIN Oggetto ON OggettoCategoria.oggetto_Id = Oggetto.oggettoId
    WHERE Categoria.nomeCategoria = "Sedie";
    
    SELECT * 
	FROM Oggetto
    JOIN OggettoCategoria ON Oggetto.oggettoId = OggettoCategoria.oggetto_Id
    JOIN Categoria ON OggettoCategoria.categoria_Id = Categoria.categoriaId
    WHERE Oggetto.nomeOggetto = "Pina";