package com.lezione29.mobilificio.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione29.mobilificio.model.Categoria;
import com.lezione29.mobilificio.services.CategoriaDAO;
import com.lezione29.mobilificio.utility.ResponsoOperazione;


@WebServlet("/inseriscicategoria")
public class InserisciCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String varNome = request.getParameter("input_nome");
		String varDescrizione = request.getParameter("input_descrizione");
		String varCodice = request.getParameter("input_codice");
		
		Categoria temp = new Categoria();
		temp.setNomeCategoria(varNome);
		temp.setDescrizioneCategoria(varDescrizione);
		temp.setCodiceCategoria(varCodice);
		
		CategoriaDAO catDao = new CategoriaDAO();
		String strErrore = "";
		try {
			catDao.insert(temp);
		} catch (SQLException e) {
			strErrore = e.getMessage();
		}
		
		PrintWriter out = response.getWriter();
		
		if(temp.getId() != null) {
			ResponsoOperazione res = new ResponsoOperazione("OK", "");
			out.print(new Gson().toJson(res));
		}
		else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
			out.print(new Gson().toJson(res));
		}
		
	}

}
