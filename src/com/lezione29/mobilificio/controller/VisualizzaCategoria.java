package com.lezione29.mobilificio.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione29.mobilificio.model.Categoria;
import com.lezione29.mobilificio.services.CategoriaDAO;
import com.lezione29.mobilificio.utility.ResponsoOperazione;

@WebServlet("/visualizzacategoria")
public class VisualizzaCategoria extends HttpServlet {

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategoriaDAO catDao = new CategoriaDAO();
		
		String codiceRicerca = request.getParameter("codice_cat") != null ? (String)request.getParameter("codice_cat") : "";
		
		try {
			
				if(codiceRicerca.equals("")) {
					ArrayList<Categoria> elencoCategorie = catDao.getAll();
					
					String risultatoJson = new Gson().toJson(elencoCategorie);
					Thread.sleep(500);
					out.print(risultatoJson);
				}
				else {
					Categoria temp = catDao.getByCode(codiceRicerca);
					out.print(new Gson().toJson(temp));
				}
		} catch (SQLException | InterruptedException e) {
			
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	//Molto compatta
			System.out.println(e.getMessage());
			
		}
		
		
	}

}
