package com.lezione29.mobilificio.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione29.mobilificio.model.Oggetto;
import com.lezione29.mobilificio.services.OggettoDAO;
import com.lezione29.mobilificio.utility.ResponsoOperazione;

@WebServlet("/visualizzaoggetti")
public class VisualizzaOggetti extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		OggettoDAO oggDao = new OggettoDAO();
		
		String codiceRicerca = request.getParameter("codice_cat") != null ? (String)request.getParameter("codice_cat") : "";
		
		try {
			
				ArrayList<Oggetto> elencoOggetti = oggDao.getAll();
					
				String risultatoJson = new Gson().toJson(elencoOggetti);
				Thread.sleep(500);
				out.print(risultatoJson);
					
		} catch (SQLException | InterruptedException e) {
			
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	//Molto compatta
			System.out.println(e.getMessage());
		}
	}

}
