package com.lezione29.mobilificio.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione29.mobilificio.model.Categoria;
import com.lezione29.mobilificio.services.CategoriaDAO;
import com.lezione29.mobilificio.utility.ResponsoOperazione;

@WebServlet("/modificacategoria")
public class ModificaCategoria extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String codiceCategoria = request.getParameter("codice_cat") != null ? (String)request.getParameter("codice_cat") : "";
		String nomeCategoria = request.getParameter("nome") != null ? (String)request.getParameter("nome") : "";
		String descrizioneCategoria = request.getParameter("descrizione") != null ? (String)request.getParameter("descrizione") : "";

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategoriaDAO catDao = new CategoriaDAO();
		
		if(codiceCategoria == "" || nomeCategoria.trim().equals("")) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "CODICE o TITOLO non inseriti!")));
			return;
		}
		
		try {
			Categoria temp = catDao.getByCode(codiceCategoria);
			
			temp.setNomeCategoria(nomeCategoria);
			temp.setDescrizioneCategoria(descrizioneCategoria);
			
			if(catDao.update(temp)) {
				out.print(new Gson().toJson(new ResponsoOperazione("OK", "")));
				return;
			}
			
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Modifica non effettuata!")));

			
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Articolo non trovato!")));
		}
	}

}
