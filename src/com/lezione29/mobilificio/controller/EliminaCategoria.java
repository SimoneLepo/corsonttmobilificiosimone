package com.lezione29.mobilificio.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione29.mobilificio.model.Categoria;
import com.lezione29.mobilificio.services.CategoriaDAO;
import com.lezione29.mobilificio.utility.ResponsoOperazione;

 
@WebServlet("/eliminacategoria")
public class EliminaCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		String codiceRicerca = request.getParameter("codice_cat") != null ? (String)request.getParameter("codice_cat") : "";

		if(codiceRicerca != "") {
			
			CategoriaDAO catDao = new CategoriaDAO();
			try {
				Categoria cat = catDao.getByCode(codiceRicerca);
				if(catDao.delete(cat)) {
					ResponsoOperazione res = new ResponsoOperazione("OK", "");
					out.print(new Gson().toJson(res));
				}
				else {
					ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nono sono riuscito a fare l'eliminazione");
					out.print(new Gson().toJson(res));
				}
			} catch (SQLException e) {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
				out.print(new Gson().toJson(res));
			}
		} else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Codice non selezionato!");
			out.print(new Gson().toJson(res));
		}
	}

}
