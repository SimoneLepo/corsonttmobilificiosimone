package com.lezione29.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lezione29.mobilificio.connessioni.ConnettoreDB;
import com.lezione29.mobilificio.model.Categoria;
import com.lezione29.mobilificio.model.Oggetto;

public class OggettoDAO implements Dao<Oggetto>{

	@Override
	public Oggetto getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Oggetto> getAll() throws SQLException {
		
		ArrayList<Oggetto> elenco = new ArrayList<Oggetto>();
		
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		
   		String query = "SELECT oggettoId, nomeOggetto, descrizioneOggetto, codiceOggetto,prezzoOggetto FROM Oggetto";
   		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
   		
   		ResultSet risultato = ps.executeQuery();
   		while(risultato.next()){
       		Oggetto temp = new Oggetto();
       		temp.setOggettoId(risultato.getInt(1));
       		temp.setNomeOggetto(risultato.getString(2));
       		temp.setDescrizioneOggetto(risultato.getString(3));
       		temp.setCodiceOggetto(risultato.getString(4));
       		temp.setPrezzoOggetto(risultato.getFloat(5));
       		elenco.add(temp);
       	}
		
		return elenco;
		
	}

	@Override
	public void insert(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
