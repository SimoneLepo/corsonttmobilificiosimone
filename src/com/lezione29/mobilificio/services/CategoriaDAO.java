package com.lezione29.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione29.mobilificio.connessioni.ConnettoreDB;
import com.lezione29.mobilificio.model.Categoria;

public class CategoriaDAO implements Dao<Categoria> {

	@Override
	public Categoria getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		
		ArrayList<Categoria> elenco = new ArrayList<Categoria>();
		
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		
   		String query = "SELECT categoriaId, nomeCategoria, descrizioneCategoria, codiceCategoria FROM Categoria";
   		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
   		
   		ResultSet risultato = ps.executeQuery();
   		while(risultato.next()){
       		Categoria temp = new Categoria();
       		temp.setId(risultato.getInt(1));
       		temp.setNomeCategoria(risultato.getString(2));
       		temp.setDescrizioneCategoria(risultato.getString(3));
       		temp.setCodiceCategoria(risultato.getString(4));
       		elenco.add(temp);
       	}
		
		return elenco;
		
	}

	@Override
	public void insert(Categoria t) throws SQLException {
		
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		
   		String query = "INSERT INTO Categoria (nomeCategoria, descrizioneCategoria, codiceCategoria) VALUES (?,?,?)";
   		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
   		ps.setString(1, t.getNomeCategoria());
   		ps.setString(2, t.getDescrizioneCategoria());
   		ps.setString(3, t.getCodiceCategoria());
   		
   		ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
	}

	@Override
	public boolean delete(Categoria t) throws SQLException {
   		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		
		String query = "DELETE FROM Categoria WHERE codiceCategoria = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getCodiceCategoria());
		
		int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public boolean update(Categoria t) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "UPDATE Categoria SET "
					+ "nomeCategoria = ?, "
					+ "descrizioneCategoria = ? "
					+ "WHERE codiceCategoria = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ps.setString(1, t.getNomeCategoria());
       	ps.setString(2, t.getDescrizioneCategoria());
       	ps.setString(3, t.getCodiceCategoria());
       	
    	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}
	
	public Categoria getByCode(String varCode) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT categoriaId, nomeCategoria, descrizioneCategoria, codiceCategoria FROM Categoria WHERE codiceCategoria = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, varCode);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

   		Categoria temp = new Categoria();
   		temp.setId(risultato.getInt(1));
   		temp.setNomeCategoria(risultato.getString(2));
   		temp.setDescrizioneCategoria(risultato.getString(3));
   		temp.setCodiceCategoria(risultato.getString(4));
       	
       	return temp;
		
	}

}
