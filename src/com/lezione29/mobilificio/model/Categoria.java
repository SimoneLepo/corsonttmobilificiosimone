package com.lezione29.mobilificio.model;

import java.util.ArrayList;

public class Categoria {

	private Integer id = 0;
	private String nomeCategoria;
	private String descrizioneCategoria;
	private String codiceCategoria;
	private ArrayList<Oggetto> oggetti;
	
	public Categoria() {
		
	}

	public ArrayList<Oggetto> getOggetti() {
		return oggetti;
	}

	public void setOggetti(ArrayList<Oggetto> oggetti) {
		this.oggetti = oggetti;
	}

	public Categoria(Integer id, String nomeCategoria, String descrizioneCategoria, String codiceCategoria,
			ArrayList<Oggetto> oggetti) {
		super();
		this.id = id;
		this.nomeCategoria = nomeCategoria;
		this.descrizioneCategoria = descrizioneCategoria;
		this.codiceCategoria = codiceCategoria;
		this.oggetti = oggetti;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNomeCategoria() {
		return nomeCategoria;
	}
	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	public String getDescrizioneCategoria() {
		return descrizioneCategoria;
	}
	public void setDescrizioneCategoria(String descrizioneCategoria) {
		this.descrizioneCategoria = descrizioneCategoria;
	}
	public String getCodiceCategoria() {
		return codiceCategoria;
	}
	public void setCodiceCategoria(String codiceCategoria) {
		this.codiceCategoria = codiceCategoria;
	}

	@Override
	public String toString() {
		return "Categoria [id=" + id + ", nomeCategoria=" + nomeCategoria + ", descrizioneCategoria="
				+ descrizioneCategoria + ", codiceCategoria=" + codiceCategoria + ", oggetti=" + oggetti + "]";
	}



	
	
	
	
	
}
