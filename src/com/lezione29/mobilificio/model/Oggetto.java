package com.lezione29.mobilificio.model;

import java.util.ArrayList;



public class Oggetto {

	private Integer oggettoId = 0;
	private String nomeOggetto;
	private String descrizioneOggetto;
	private String codiceOggetto;
	private float prezzoOggetto;
	private ArrayList<Categoria> categorie;
	
	public Oggetto() {
		
	}

	public Oggetto(Integer oggettoId, String nomeOggetto, String descrizioneOggetto, String codiceOggetto,
			float prezzoOggetto, ArrayList<Categoria> categorie) {
		super();
		this.oggettoId = oggettoId;
		this.nomeOggetto = nomeOggetto;
		this.descrizioneOggetto = descrizioneOggetto;
		this.codiceOggetto = codiceOggetto;
		this.prezzoOggetto = prezzoOggetto;
		this.categorie = categorie;
	}

	public Integer getOggettoId() {
		return oggettoId;
	}

	public void setOggettoId(Integer oggettoId) {
		this.oggettoId = oggettoId;
	}

	public String getNomeOggetto() {
		return nomeOggetto;
	}

	public void setNomeOggetto(String nomeOggetto) {
		this.nomeOggetto = nomeOggetto;
	}

	public String getDescrizioneOggetto() {
		return descrizioneOggetto;
	}

	public void setDescrizioneOggetto(String descrizioneOggetto) {
		this.descrizioneOggetto = descrizioneOggetto;
	}

	public float getPrezzoOggetto() {
		return prezzoOggetto;
	}

	public void setPrezzoOggetto(float prezzoOggetto) {
		this.prezzoOggetto = prezzoOggetto;
	}


	public ArrayList<Categoria> getCategorie() {
		return categorie;
	}


	public void setCategorie(ArrayList<Categoria> categorie) {
		this.categorie = categorie;
	}

	public String getCodiceOggetto() {
		return codiceOggetto;
	}

	public void setCodiceOggetto(String codiceOggetto) {
		this.codiceOggetto = codiceOggetto;
	}

	@Override
	public String toString() {
		return "Oggetto [oggettoId=" + oggettoId + ", nomeOggetto=" + nomeOggetto + ", descrizioneOggetto="
				+ descrizioneOggetto + ", codiceOggetto=" + codiceOggetto + ", prezzoOggetto=" + prezzoOggetto
				+ ", categorie=" + categorie + "]";
	}



	
	
	
	
}
