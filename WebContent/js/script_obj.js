function tabellaOggetti(obj_oggetto){
	
	let risultato = '<tr data-identificatore="' + obj_oggetto.codiceOggetto + '">'
	risultato += '    <td>' + obj_oggetto.codiceOggetto + '</td>';
	risultato += '    <td>' + obj_oggetto.nomeOggetto + '</td>';
	risultato += '    <td>' + obj_oggetto.descrizioneOggetto + '</td>';
	risultato += '    <td>' + obj_oggetto.prezzoOggetto + '</td>';
	risultato += '    <td><button type="button" class="btn btn-primary btn-block" onclick="mostraCategorie(this)"><i class="fas fa-info-circle"></i></button></td>';
	risultato += '</tr>';
	
	return risultato;
	
}

function stampaTabella(arr_oggetti){
	let contenuto = "";
	
	for(let i=0; i<arr_oggetti.length; i++){
		contenuto += tabellaOggetti(arr_oggetti[i]);
	}
	
	$("#contenuto-richiesta-oggetti").html(contenuto);
}

function aggiornaTabella(){
	$.ajax(
		{
			url:"http://localhost:8080/CorsoNTTMobilificio/visualizzaoggetti",
			method: "POST",
			success: function(ris_success){
				stampaTabella(ris_success);
			},
			error: function(ris_error){
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}

$(document).ready(
	function(){
	
		$("#cta-visualizzaogg").click(
					function(){
						aggiornaTabella();
						console.log("Fatto!");
					}
			);
			
	}		
);			