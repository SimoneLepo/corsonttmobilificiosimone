
function tabellaCategorie(obj_categoria){
	
	let risultato = '<tr data-identificatore="' + obj_categoria.codiceCategoria + '">'
	risultato += '    <td>' + obj_categoria.codiceCategoria + '</td>';
	risultato += '    <td>' + obj_categoria.nomeCategoria + '</td>';
	risultato += '    <td>' + obj_categoria.descrizioneCategoria + '...</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaCategoria(this)"><i class="fas fa-trash"></i></button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaCategoria(this)"><i class="fas fa-pencil"></i></button></td>';
	risultato += '</tr>';
	
	return risultato;
	
}

function stampaTabella(arr_categorie){
	let contenuto = "";
	
	for(let i=0; i<arr_categorie.length; i++){
		contenuto += tabellaCategorie(arr_categorie[i]);
	}
	
	$("#contenuto-richiesta").html(contenuto);
}

function aggiornaTabella(){
	$.ajax(
		{
			url:"http://localhost:8080/CorsoNTTMobilificio/visualizzacategoria",
			method: "POST",
			success: function(ris_success){
				stampaTabella(ris_success);
			},
			error: function(ris_error){
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}

function eliminaCategoria(objButton){
	let codiceSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTMobilificio/eliminacategoria",
				method: "POST",
				data: {
					codice_cat: codiceSelezionato
				},
				success: function(responso){			//Senza JSON Parse perché la response è in application/json
					switch(responso.risultato){
					case "OK":
						aggiornaTabella();
						alert("Eliminato con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI ELIMINAZIONE :(\n" + responso.dettaglio);
						break;
					}
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}	

function modificaCategoria(objButton){
	let codiceSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTMobilificio/visualizzacategoria",
				method: "POST",
				data: {
					codice_cat: codiceSelezionato
				},
				success: function(responso){			//Senza JSON Parse perché la response è in application/json
					console.log(responso);
					$("#nome_edit").val(responso.nomeCategoria);
					$("#descrizione_edit").val(responso.descrizioneCategoria);
					$("#modaleModifica").data("identificatore", codiceSelezionato);

					$("#modaleModifica").modal("show");
					
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}

$(document).ready(
	function(){
	
		$("#cta-visualizza").click(
					function(){
						aggiornaTabella();
						console.log("Fatto!");
					}
			);
			
		$("#cta-aggiungi").click(
				function(){
					$("#modaleInserimento").modal("show");
				}
			);
			
		$("#cta-inserisci").click(
				function(){
					
					let varNome = $("#nome").val();
					let varDescrizione = $("#descrizione").val();
					let varCodice = $("#codice").val();
					
					$.ajax(
						{
							url:"http://localhost:8080/CorsoNTTMobilificio/inseriscicategoria",
							method: "POST",
							data: {
								input_nome: varNome,
								input_descrizione: varDescrizione,
								input_codice: varCodice
							}, 
							success: function(risultato){
								console.log("SONO IN SUCCESS");
								let risJson = JSON.parse(risultato);
								
								switch(risJson.risultato){
								case "OK": 
									aggiornaTabella();
									$("#modaleInserimento").modal("toggle");
									alert("Inserito con successo");
									break;
								case "ERRORE":
									alert("Errore di inserimento\n" + risJson.dettaglio);		
									break;
								}
							},
							error: function(errore){
								console.log("SONO IN ERROR");
								let errJson = JSON.parse(errore);
								console.log(errJson);
							}
						}
					);
				}	
			);
			
			$("#cta-modifica").click(
					function(){
						let codiceSelezionato = $("#modaleModifica").data("identificatore");

						$.ajax(
								{
									url: "http://localhost:8080/CorsoNTTMobilificio/modificacategoria",
									method: "POST",
									data: {
										codice_cat: codiceSelezionato,
										nome: $("#nome_edit").val(),
										descrizione: $("#descrizione_edit").val()
									},
									success: function(responso){
										switch(responso.risultato){
										case "OK":
											$("#modaleModifica").modal("toggle");
											aggiornaTabella();
											break;
										case "ERRORE":
											alert(responso.risultato + "\n" + responso.dettaglio);
											break;
										}
									},
									error: function(errore){
										console.log(errore);
									}
								}
						);
						
					}
			);		
			
	}
);